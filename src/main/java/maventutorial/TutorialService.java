package maventutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("maventutorial.model")
@EntityScan("maventutorial.entity")
@SpringBootApplication
public class TutorialService {
    public static void main(String[] args) {

        SpringApplication.run(TutorialService.class,args);
    }


}
