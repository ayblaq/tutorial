package maventutorial.controller;

import maventutorial.entity.Building;
import maventutorial.error.BuildingNotFoundException;
import maventutorial.model.buildingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/buildings")
public class buildingController {
    @Autowired
    private buildingRepository buildingrepository;

    @GetMapping
    public Iterable findAll() {
        return buildingrepository.findAll();
    }

    @GetMapping("/client/{clientId}")
    public ResponseEntity findByclientId(@PathVariable int clientId) {
        List<Building> byClientId = buildingrepository.findByclientId(clientId);
        return new ResponseEntity(byClientId, HttpStatus.OK);
    }

    @GetMapping("/{BUIL_ID}")
    public Building findByOne(@PathVariable Long BUIL_ID) throws BuildingNotFoundException{
        Building building = buildingrepository.findById(BUIL_ID);
        return new ResponseEntity(building,HttpStatus.OK)
                .orElseThrow(BuildingNotFoundException::new);
//        return buildingrepository.findById(BUIL_ID)
//                .orElseThrow(BuildingNotFoundException::new);
    }
}
