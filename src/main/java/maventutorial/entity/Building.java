package maventutorial.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
public class Building {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long BUIL_ID;

    @Column(nullable = false, name="CLIE_ID")
    private int clientId;

    @Column(nullable = false)
    private int BUTY_ID;

    @Length(max = 255)
    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private int MACH_ID;

    @Column(nullable = false)
    private int LOCA_ID;

    @Column()
    private int update_user;

    @Column()
    private Integer update_time;

    public Building(){
        super();
    }

    public Building(long BUIL_ID, int clientId, int BUTY_ID,String address, int MACH_ID, int LOCA_ID, int update_user, Integer update_time){

        super();
        this.BUIL_ID = BUIL_ID;
        this.clientId = clientId;
        this.BUTY_ID = BUTY_ID;
        this.address = address;
        this.MACH_ID = MACH_ID;
        this.LOCA_ID = LOCA_ID;
        this.update_time = update_time;
        this.update_user = update_user;
    }

    public long getBUIL_ID() {
        return this.BUIL_ID;
    }

    public int getClientId()  {
        return this.clientId;
    }

    public int getBUTY_ID(){
        return this.BUTY_ID;
    }

    public String getAddress() {
        return this.address;
    }

    public int getMACH_ID(){
        return this.MACH_ID;
    }

    public int getLOCA_ID(){
        return this.LOCA_ID;
    }

    public int getUpdate_user(){
        return this.update_user;
    }

    public Integer getUpdate_time(){
        return this.update_time;
    }
}
