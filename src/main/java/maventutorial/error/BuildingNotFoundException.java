package maventutorial.error;

public class BuildingNotFoundException  extends Exception {

    public BuildingNotFoundException(String message, Throwable cause) {

        super(message, cause);
    }
}