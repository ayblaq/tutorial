package maventutorial.model;

import maventutorial.entity.Building;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface buildingRepository extends CrudRepository<Building, Long> {
    List<Building> findByclientId(int clientId);
}
